/*
 * SPDX license identifier: MIT
 *
 * Copyright (c) 2020-2025 Alin Popa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * \author Alin Popa <alin.popa@triboo-tech.ro>
 * \file cdi-application.c
 */

#include "cdi-application.h"
#include "cdm-utils.h"

#include <fcntl.h>
#include <glib/gstdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

CdiApplication *
cdi_application_new (const gchar *config, GError **error)
{
  CdiApplication *app = g_new0 (CdiApplication, 1);

  g_assert (app);
  g_assert (error);

  g_ref_count_init (&app->rc);

  /* construct options noexept */
  app->options = cdm_options_new (config);

  /* construct journal and return if an error is set */
  app->journal = cdi_journal_new (app->options, error);

  return app;
}

CdiApplication *
cdi_application_ref (CdiApplication *app)
{
  g_assert (app);
  g_ref_count_inc (&app->rc);
  return app;
}

void
cdi_application_unref (CdiApplication *app)
{
  g_assert (app);

  if (g_ref_count_dec (&app->rc) == TRUE)
    {
      if (app->journal != NULL)
        cdi_journal_unref (app->journal);

      if (app->options != NULL)
        cdm_options_unref (app->options);

      g_free (app);
    }
}

void
cdi_application_list_entries (CdiApplication *app)
{
  g_assert (app);
  cdi_journal_list_entries (app->journal, NULL);
}

void
cdi_application_list_content (CdiApplication *app, const gchar *fpath)
{
  g_autoptr (CdiArchive) archive = NULL;
  CdmStatus status = CDM_STATUS_OK;

  g_assert (app);
  g_assert (fpath);

  archive = cdi_archive_new ();

  if (g_access (fpath, R_OK) == 0)
    status = cdi_archive_read_open (archive, fpath);
  else
    {
      g_autofree gchar *opt_coredir = NULL;
      g_autofree gchar *dbpath = NULL;

      opt_coredir = cdm_options_string_for (app->options, KEY_CRASHDUMP_DIR);
      dbpath = g_build_filename (opt_coredir, fpath, NULL);

      status = cdi_archive_read_open (archive, dbpath);
    }

  if (status == CDM_STATUS_OK)
    (void)cdi_archive_list_stdout (archive);
  else
    g_print ("Cannot open file: %s\n", fpath);
}

void
cdi_application_print_info (CdiApplication *app, const gchar *fpath)
{
  g_autoptr (CdiArchive) archive = NULL;
  CdmStatus status = CDM_STATUS_OK;

  g_assert (app);
  g_assert (fpath);

  archive = cdi_archive_new ();

  if (g_access (fpath, R_OK) == 0)
    status = cdi_archive_read_open (archive, fpath);
  else
    {
      g_autofree gchar *opt_coredir = NULL;
      g_autofree gchar *dbpath = NULL;

      opt_coredir = cdm_options_string_for (app->options, KEY_CRASHDUMP_DIR);
      dbpath = g_build_filename (opt_coredir, fpath, NULL);

      status = cdi_archive_read_open (archive, dbpath);
    }

  if (status == CDM_STATUS_OK)
    (void)cdi_archive_print_info (archive);
  else
    g_print ("Cannot open file: %s\n", fpath);
}

void
cdi_application_print_epilog (CdiApplication *app, const gchar *fpath)
{
  g_autoptr (CdiArchive) archive = NULL;
  CdmStatus status = CDM_STATUS_OK;

  g_assert (app);
  g_assert (fpath);

  archive = cdi_archive_new ();

  if (g_access (fpath, R_OK) == 0)
    status = cdi_archive_read_open (archive, fpath);
  else
    {
      g_autofree gchar *opt_coredir = NULL;
      g_autofree gchar *dbpath = NULL;

      opt_coredir = cdm_options_string_for (app->options, KEY_CRASHDUMP_DIR);
      dbpath = g_build_filename (opt_coredir, fpath, NULL);

      status = cdi_archive_read_open (archive, dbpath);
    }

  if (status == CDM_STATUS_OK)
    (void)cdi_archive_print_epilog (archive);
  else
    g_print ("Cannot open file: %s\n", fpath);
}

void
cdi_application_print_file (CdiApplication *app, const gchar *fname, const gchar *fpath)
{
  g_autoptr (CdiArchive) archive = NULL;
  CdmStatus status = CDM_STATUS_OK;

  g_assert (app);
  g_assert (fpath);

  archive = cdi_archive_new ();

  if (g_access (fpath, R_OK) == 0)
    status = cdi_archive_read_open (archive, fpath);
  else
    {
      g_autofree gchar *opt_coredir = NULL;
      g_autofree gchar *dbpath = NULL;

      opt_coredir = cdm_options_string_for (app->options, KEY_CRASHDUMP_DIR);
      dbpath = g_build_filename (opt_coredir, fpath, NULL);

      status = cdi_archive_read_open (archive, dbpath);
    }

  if (status == CDM_STATUS_OK)
    (void)cdi_archive_print_file (archive, fname);
  else
    g_print ("Cannot open file: %s\n", fpath);
}

void
cdi_application_extract_coredump (CdiApplication *app, const gchar *fpath)
{
  g_autoptr (CdiArchive) archive = NULL;
  CdmStatus status = CDM_STATUS_OK;

  g_assert (app);
  g_assert (fpath);

  archive = cdi_archive_new ();

  if (g_access (fpath, R_OK) == 0)
    status = cdi_archive_read_open (archive, fpath);
  else
    {
      g_autofree gchar *opt_coredir = NULL;
      g_autofree gchar *dbpath = NULL;

      opt_coredir = cdm_options_string_for (app->options, KEY_CRASHDUMP_DIR);
      dbpath = g_build_filename (opt_coredir, fpath, NULL);

      status = cdi_archive_read_open (archive, dbpath);
    }

  if (status == CDM_STATUS_OK)
    {
      g_autofree gchar *cwd = g_get_current_dir ();
      (void)cdi_archive_extract_coredump (archive, cwd);
    }
  else
    g_print ("Cannot open file: %s\n", fpath);
}

void
cdi_application_print_backtrace (CdiApplication *app, gboolean all, const gchar *fpath)
{
  g_autoptr (CdiArchive) archive = NULL;
  CdmStatus status = CDM_STATUS_OK;

  g_assert (app);
  g_assert (fpath);

  archive = cdi_archive_new ();

  if (g_access (fpath, R_OK) == 0)
    status = cdi_archive_read_open (archive, fpath);
  else
    {
      g_autofree gchar *opt_coredir = NULL;
      g_autofree gchar *dbpath = NULL;

      opt_coredir = cdm_options_string_for (app->options, KEY_CRASHDUMP_DIR);
      dbpath = g_build_filename (opt_coredir, fpath, NULL);

      status = cdi_archive_read_open (archive, dbpath);
    }

  if (status == CDM_STATUS_OK)
    (void)cdi_archive_print_backtrace (archive, all);
  else
    g_print ("Cannot open file: %s\n", fpath);
}
