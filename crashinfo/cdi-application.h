/*
 * SPDX license identifier: MIT
 *
 * Copyright (c) 2020-2025 Alin Popa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * \author Alin Popa <alin.popa@triboo-tech.ro>
 * \file cdi-application.h
 */

#pragma once

#include "cdi-archive.h"
#include "cdi-journal.h"
#include "cdm-defaults.h"
#include "cdm-logging.h"
#include "cdm-options.h"
#include "cdm-types.h"

#include <glib.h>
#include <stdlib.h>

G_BEGIN_DECLS

/**
 * @brief Crashinfo application object referencing main objects
 */
typedef struct _CdiApplication
{
  CdmOptions *options;
  CdiJournal *journal;
  grefcount rc;
} CdiApplication;

/**
 * @brief Create a new CdiApplication object
 * @param config Full path to the configuration file
 * @param error An error object must be provided. If an error occurs during
 * initialization the error is reported and application should not use the
 * returned object. If the error is set the object is invalid and needs to be
 * released.
 */
CdiApplication *cdi_application_new (const gchar *config, GError **error);

/**
 * @brief Aquire CdiApplication object
 * @param app The object to aquire
 * @return The aquiered app object
 */
CdiApplication *cdi_application_ref (CdiApplication *app);

/**
 * @brief Release a CdiApplication object
 * @param app The cdi application object to release
 */
void cdi_application_unref (CdiApplication *app);

/**
 * @brief List crash entries
 * @param app The cdi application
 */
void cdi_application_list_entries (CdiApplication *app);

/**
 * @brief List crash archive content
 * @param app The cdi application
 * @param fpath Input file path
 */
void cdi_application_list_content (CdiApplication *app, const gchar *fpath);

/**
 * @brief Prnt info file
 * @param app The cdi application
 * @param fpath Input file path
 */
void cdi_application_print_info (CdiApplication *app, const gchar *fpath);

/**
 * @brief Print epilog file
 * @param app The cdi application
 * @param fpath Input file path
 */
void cdi_application_print_epilog (CdiApplication *app, const gchar *fpath);

/**
 * @brief Extract coredump file
 * @param app The cdi application
 * @param fpath Input file path
 */
void cdi_application_extract_coredump (CdiApplication *app, const gchar *fpath);

/**
 * @brief Print content of a file in the archive
 * @param app The cdi application
 * @param fname Input file name
 * @param fpath Input file path
 */
void cdi_application_print_file (CdiApplication *app, const gchar *fname, const gchar *fpath);

/**
 * @brief Print backtrace
 * @param app The cdi application
 * @param all If true print backtrace for all threads
 * @param fpath Input file path
 */
void cdi_application_print_backtrace (CdiApplication *app, gboolean all, const gchar *fpath);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (CdiApplication, cdi_application_unref);

G_END_DECLS
