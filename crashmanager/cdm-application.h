/*
 * SPDX license identifier: MIT
 *
 * Copyright (c) 2020-2025 Alin Popa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * \author Alin Popa <alin.popa@triboo-tech.ro>
 * \file cdm-application.h
 */

#pragma once

#include "cdm-defaults.h"
#include "cdm-elogsrv.h"
#include "cdm-janitor.h"
#include "cdm-journal.h"
#include "cdm-logging.h"
#include "cdm-options.h"
#include "cdm-server.h"
#include "cdm-transfer.h"
#include "cdm-types.h"
#ifdef WITH_SYSTEMD
#include "cdm-sdnotify.h"
#endif
#ifdef WITH_GENIVI_NSM
#include "cdm-lifecycle.h"
#endif
#ifdef WITH_DBUS_SERVICES
#include "cdm-dbusown.h"
#endif

#include <glib.h>
#include <stdlib.h>

G_BEGIN_DECLS

/**
 * @brief CdmApplication application object referencing main objects
 */
typedef struct _CdmApplication
{
  CdmOptions *options;
  CdmServer *server;
  CdmELogSrv *elogsrv;
  CdmJanitor *janitor;
  CdmJournal *journal;
#ifdef WITH_SYSTEMD
  CdmSDNotify *sdnotify;
#endif
#ifdef WITH_GENIVI_NSM
  CdmLifecycle *lifecycle;
#endif
#ifdef WITH_DBUS_SERVICES
  CdmDBusOwn *dbusown;
#endif
  CdmTransfer *transfer;
  GMainLoop *mainloop;
  grefcount rc;
} CdmApplication;

/**
 * @brief Create a new CdmApplication object
 * @param config Full path to the configuration file
 * @param error An error object must be provided. If an error occurs during
 * initialization the error is reported and application should not use the
 * returned object. If the error is set the object is invalid and needs to be
 * released.
 */
CdmApplication *cdm_application_new (const gchar *config, GError **error);

/**
 * @brief Aquire CdmApplication object
 * @param app The object to aquire
 * @return The aquiered CdmApplication object
 */
CdmApplication *cdm_application_ref (CdmApplication *app);

/**
 * @brief Release a CdmApplication object
 * @param app The cdm application object to release
 */
void cdm_application_unref (CdmApplication *app);

/**
 * @brief Execute cdm application
 * @param app The cdm application object
 * @return If run was succesful CDM_STATUS_OK is returned
 */
CdmStatus cdm_application_execute (CdmApplication *app);

/**
 * @brief Get main event loop reference
 * @param app The cdm application object
 * @return A pointer to the main event loop
 */
GMainLoop *cdm_application_get_mainloop (CdmApplication *app);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (CdmApplication, cdm_application_unref);

G_END_DECLS
