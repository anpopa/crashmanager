/*
 * SPDX license identifier: MIT
 *
 * Copyright (c) 2020-2025 Alin Popa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * \author Alin Popa <alin.popa@triboo-tech.ro>
 * \file cdm-elogclt.h
 */

#pragma once

#include "cdh-elogmsg.h"
#include "cdm-journal.h"
#include "cdm-types.h"

#include <glib.h>

G_BEGIN_DECLS

/**
 * @brief The CdmELogClt opaque data structure
 */
typedef struct _CdmELogClt
{
  GSource source;                   /**< Event loop source */
  gpointer tag;                     /**< Unix server socket tag  */
  grefcount rc;                     /**< Reference counter variable  */
  gint sockfd;                      /**< Module file descriptor (client fd) */
  CdmJournal *journal;              /**< Own a reference to the journal object */
  CdmELogMessageType last_msg_type; /**< Last processed message type */
  int64_t process_pid;              /**< Process PID*/
  int64_t process_sig;              /**< Process exit signal*/
} CdmELogClt;

/*
 * @brief Create a new client object
 * @param clientfd Socket file descriptor accepted by the server
 * @param transfer A pointer to the CdmTransfer object created by the main application
 * @param journal A pointer to the CdmJournal object created by the main application
 * @return On success return a new CdmELogClt object
 */
CdmELogClt *cdm_elogclt_new (gint clientfd, CdmJournal *journal);

/**
 * @brief Aquire client object
 * @param client Pointer to the client object
 * @return The referenced client object
 */
CdmELogClt *cdm_elogclt_ref (CdmELogClt *client);

/**
 * @brief Release client object
 * @param client Pointer to the client object
 */
void cdm_elogclt_unref (CdmELogClt *client);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (CdmELogClt, cdm_elogclt_unref);

G_END_DECLS
