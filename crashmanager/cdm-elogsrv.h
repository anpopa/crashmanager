/*
 * SPDX license identifier: MIT
 *
 * Copyright (c) 2020-2025 Alin Popa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * \author Alin Popa <alin.popa@triboo-tech.ro>
 * \file cdm-elogsrv.h
 */

#pragma once

#include "cdm-journal.h"
#include "cdm-options.h"
#include "cdm-types.h"

#include <glib.h>

G_BEGIN_DECLS

/**
 * @brief The CdmELogSrv opaque data structure
 */
typedef struct _CdmELogSrv
{
  GSource source;      /**< Event loop source */
  grefcount rc;        /**< Reference counter variable  */
  gpointer tag;        /**< Unix elogsrv socket tag  */
  gint sockfd;         /**< Module file descriptor (elogsrv listen fd) */
  CdmOptions *options; /**< Own reference to global options */
  CdmJournal *journal; /**< Own a reference to the journal object */
} CdmELogSrv;

/*
 * @brief Create a new elogsrv object
 * @param options A pointer to the CdmOptions object created by the main application
 * @param journal A pointer to the CdmJournal object created by the main application
 * @return On success return a new CdmELogSrv object otherwise return NULL
 */
CdmELogSrv *cdm_elogsrv_new (CdmOptions *options, CdmJournal *journal, GError **error);

/**
 * @brief Aquire elogsrv object
 * @param elogsrv Pointer to the elogsrv object
 * @return The elogsrv object
 */
CdmELogSrv *cdm_elogsrv_ref (CdmELogSrv *elogsrv);

/**
 * @brief Start the elogsrv an listen for clients
 * @param elogsrv Pointer to the elogsrv object
 * @return If elogsrv starts listening the function return CDM_STATUS_OK
 */
CdmStatus cdm_elogsrv_bind_and_listen (CdmELogSrv *elogsrv);

/**
 * @brief Release elogsrv object
 * @param elogsrv Pointer to the elogsrv object
 */
void cdm_elogsrv_unref (CdmELogSrv *elogsrv);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (CdmELogSrv, cdm_elogsrv_unref);

G_END_DECLS
